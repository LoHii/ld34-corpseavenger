﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIFade : MonoBehaviour {

    public bool menuCare = true;
    public bool scaleDown = false;
    public bool curtain;
    public float fadeDuration;
    float alpha;
    Color myCol;
    Image myImage;
    Text myText;
    bool isImage;
    Color colHolder;
    bool fadingIn = true;
    MenuController menu;
    Vector3 scale;

    void Awake() {
        if (menuCare) {

            menu = GameObject.Find("MenuController").GetComponent<MenuController>();
            fadeDuration = menu.fadeTime;
        }
        myImage = GetComponent<Image>();
        myText = GetComponent<Text>();

        if (curtain) {
            alpha = 1;
            fadingIn = false;
        }

        if (myImage == null) {
            myCol = myText.color;
            colHolder = new Color(myCol.r, myCol.g, myCol.b, alpha);
            myText.color = colHolder;
        } else {
            myCol = myImage.color;
            colHolder = new Color(myCol.r, myCol.g, myCol.b, alpha);
            myImage.color = colHolder;
            scale = myImage.rectTransform.localScale;
        }
    }

    void Update() {
        alpha = (fadingIn) ? Mathf.Min(1, alpha + Time.deltaTime / fadeDuration) : Mathf.Max(0, alpha - Time.deltaTime / fadeDuration);
        colHolder = new Color(myCol.r, myCol.g, myCol.b, alpha);

        if (scaleDown)
            myImage.rectTransform.localScale = scale * (4 - alpha*3);

        if (myImage == null)
            myText.color = colHolder;
        else
            myImage.color = colHolder;
        if (menuCare)
            fadingIn = (curtain) ? !menu.fading : menu.fading;
    }

    public void Fade(bool fadeIn) {
        fadingIn = fadeIn;
    }
}
