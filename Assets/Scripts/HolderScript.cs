﻿using UnityEngine;
using System.Collections;

public class HolderScript : MonoBehaviour {

    public ParticleSystem big;
    public ParticleSystem small;
    public ParticleSystem line;
    public HingeJoint hj;
    float particleCount;

    void Start() {
        particleCount = line.emission.rate.constantMin;
        if (Vector3.Distance(hj.connectedBody.position, transform.position) < 0.5f)
            line.Stop();
    }

    public void Select() {
        big.Play();
        small.Stop();
    }

    void Update() {
        SetLine();
    }

    void SetLine() {
        Vector3 anchorWorld = hj.connectedBody.position;
        line.transform.rotation = Quaternion.LookRotation(anchorWorld - line.transform.position, Vector3.up)*Quaternion.Euler(0,90,0);
        line.transform.position = transform.position + (anchorWorld - transform.position) / 2;
        ParticleSystem.MinMaxCurve r = line.emission.rate;
        ParticleSystem.ShapeModule s = line.shape;
        s.radius = Vector3.Distance(anchorWorld, transform.position)/2;
        r.constantMin = particleCount * line.shape.radius;
    }

    public void Unselect() {
        small.Play();
        big.Stop();
    }

    public void Die() {
        big.Stop();
        big.transform.parent = null;
        Destroy(big.gameObject, big.startLifetime);
        small.Stop();
        small.transform.parent = null;
        Destroy(small.gameObject, small.startLifetime);
        line.startLifetime = big.startLifetime;
        line.Emit((int)line.emission.rate.constantMin);
        line.Stop();
        line.transform.parent = null;
        Destroy(small.gameObject, small.startLifetime);
        Destroy(this.gameObject);
    }
}
