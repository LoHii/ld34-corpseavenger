﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

    public float timeBetweenInputs;
    float timeInput;
    public float fadeTime;
    List<GameObject> mainButtons;
    List<GameObject> levelButtons;
    EventSystem es;
    List<GameObject> current;
    int index;
    [HideInInspector]
    public bool fading = true;
    int targetLevel = -1;
    UIFade curtain;
    int maxLevel;
    public Sprite lockedSprite;

    void Start() {
        maxLevel = PlayerPrefs.GetInt("Levels");
        if (maxLevel == 0) {
            maxLevel = 1;
            PlayerPrefs.SetInt("Levels", maxLevel);
        }
        curtain = GameObject.Find("Curtain").GetComponent<UIFade>();
        mainButtons = GetButtonChildren("MainMenu");
        levelButtons = GetButtonChildren("LevelSelect");
        current = mainButtons;
        es = GetComponent<EventSystem>();
        es.SetSelectedGameObject(current[0].gameObject);
        levelButtons[0].transform.parent.gameObject.SetActive(false);
    }

    void Update() {
        if (targetLevel == -1) {
            if (Input.GetButtonUp("Button1"))
                timeInput = 0;

            if (timeInput > 0)
                timeInput = Mathf.Max(0, timeInput - Time.deltaTime);

            if (Input.GetButton("Button1") && timeInput == 0) {
                if (index == current.Count - 1) {
                    index = 0;
                } else {
                    index++;
                    if (current == levelButtons && index > maxLevel-1) {
                        index = current.Count - 1;
                    }
                }
                es.SetSelectedGameObject(current[index].gameObject);
                timeInput = timeBetweenInputs;
                AudioLord.Play("Switch");
            }

            if (Input.GetButtonDown("Button2")) {
                es.currentSelectedGameObject.GetComponent<Button>().onClick.Invoke();
                timeInput = 0;

                AudioLord.Play("Ding");
            }
        }
    }

    List<GameObject> GetButtonChildren(string parentName) {
        List<GameObject> kids = new List<GameObject>();
        Transform buttonParent = GameObject.Find(parentName).transform;

        for (int i = 0; i < buttonParent.childCount; i++) {
            Transform c = buttonParent.GetChild(i);

            if (c.GetComponent<Button>() != null) {
                kids.Add(c.gameObject);
            }
        }

        return kids;
    }

    public void LevelSelect() {
        fading = false;
        Invoke("GoLevel", fadeTime);
    }

    void GoLevel() {
        mainButtons[0].transform.parent.gameObject.SetActive(false);
        levelButtons[0].transform.parent.gameObject.SetActive(true);
        current = levelButtons;
        es.SetSelectedGameObject(current[0]);
        index = 0;
        fading = true;
        for(int i = 0; i < levelButtons.Count-1; i++) {
            if (i > maxLevel-1) {
                levelButtons[i].transform.GetChild(0).gameObject.SetActive(false);
                levelButtons[i].GetComponent<Image>().sprite = lockedSprite;
            }
        }
    }

    public void MainMenu() {
        fading = false;
        Invoke("GoMenu", fadeTime);
    }

    void GoMenu() {
        mainButtons[0].transform.parent.gameObject.SetActive(true);
        levelButtons[0].transform.parent.gameObject.SetActive(false);
        current = mainButtons;
        es.SetSelectedGameObject(current[0]);
        index = 0;
        fading = true;
    }

    public void LoadLevel(int level) {
        PlayerPrefs.SetInt("Credits", 1);
        Invoke("StartLevel", curtain.fadeDuration);
        targetLevel = level;
        curtain.Fade(true);
    }

    void StartLevel() {
        PlayerPrefs.SetInt("CurrentLevel", targetLevel);
        SceneManager.LoadScene(targetLevel);
    }
}