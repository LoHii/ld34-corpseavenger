﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class CreditScript : MonoBehaviour {

    public float timer;
    int phase;
    public AnimationCurve curve;
    int mode;
    public GameObject button;
    public UIFade curtain;

	void Start () {
        mode = PlayerPrefs.GetInt("Credits");
        if (mode == 1) {
            phase = 3;
            transform.rotation = Quaternion.Euler(0, -25, 0);
            Invoke("SetButton", curtain.fadeDuration);
            Destroy(GameObject.Find("crateEnd"));
        }
	}
	
	void Update () {
        if (phase != 3)
            timer -= Time.deltaTime / Time.timeScale;
       else if (Input.GetButtonDown("Button2"))
            Press();

        if (timer < 5.686944f && phase == 0) {
            AudioLord.Play("Crash");
            phase++;
        }

        if (timer < 0) {
            phase++;
            timer += 1.5f;
            if (phase == 3)
                SetButton();
        }
	    
        if (phase == 2) {
            transform.rotation = Quaternion.Lerp(Quaternion.Euler(0, -25, 0),Quaternion.Euler(0, 57.5621f,0),curve.Evaluate(timer/1.5f));
        }
	}

    void SetButton() {
        button.SetActive(true);
    }

    public void Press() {
        if (phase == 3) {
            curtain.Fade(true);
            phase++;
            Invoke("Menu", curtain.fadeDuration);
            AudioLord.Play("Ding");
        }
    }

    void Menu() {
        SceneManager.LoadScene(0);
    }
}
