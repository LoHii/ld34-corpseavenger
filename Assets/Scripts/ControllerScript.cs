﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ControllerScript : MonoBehaviour {

    List<HolderScript> hsList = new List<HolderScript>();
    int index;
    bool press;
    float restart;
    float menu;
    public Image menuImage;
    public Image restartImage;
    public Transform holders;
    public enum GameState { FadeIn, Begin, Flung, Hit, End, FadeAway };
    public GameState state;
    public Transform end;
    public Image curtain;
    float alpha = 1;
    int level;
    GameObject[] cameras = new GameObject[3];
    GameObject menuText;
    GameObject restartText;
    AudioSource loop;

    void Start() {
        HolderScript[] hsArray = holders.GetComponentsInChildren<HolderScript>();
        if (hsArray.Length != 0) {
            hsArray[0].Select();
            hsList.Add(hsArray[0]);
        }
        for (int i = 1; i < hsArray.Length; i++) {
            hsArray[i].Unselect();
            hsList.Add(hsArray[i]);
        }
        index = 0;
        loop = AudioLord.PlayLoop("Loop");
        curtain.gameObject.SetActive(true);
        curtain.color = Color.black;
        cameras[0] = GameObject.Find("Main Camera");
        cameras[1] = GameObject.Find("Make Camera");
        cameras[2] = GameObject.Find("Hit Camera");
        cameras[1].SetActive(false);
        cameras[2].SetActive(false);
        menuText = restartImage.transform.parent.GetChild(2).gameObject;
        restartText = restartImage.transform.parent.GetChild(3).gameObject;
    }

    void Update() {
        if (state == GameState.FadeIn) {
            alpha = Mathf.Max(0, alpha - Time.deltaTime);
            curtain.color = Color.Lerp(Color.clear, Color.black, alpha);

            if (alpha == 0) {
                curtain.gameObject.SetActive(false);
                state = GameState.Begin;
            }
        }

        if (state == GameState.Begin) {
            /*if (Input.GetKeyDown(KeyCode.S) && hsList.Count > 1)
                Next();*/

            if (!press && Input.GetKeyDown(KeyCode.D) && hsList.Count != 0)
                Kill();

        }

        if (!Input.GetKey(KeyCode.D))
            press = false;

        if (state == GameState.Flung || state == GameState.End) {
            if (Input.GetKey(KeyCode.D) && !press) {
                restart += Time.deltaTime / Time.timeScale;
                restartText.SetActive(false);

                if (restart >= 1) {
                    level = (state == GameState.Flung) ? SceneManager.GetActiveScene().buildIndex : SceneManager.GetActiveScene().buildIndex + 1;
                    AudioLord.DestroyLoop(loop, 1.0f);
                    Fade();
                    AudioLord.Play("Ding");
                }
            }

            if (Input.GetKey(KeyCode.S)) {
                menu += Time.deltaTime / Time.timeScale;
                menuText.SetActive(false);

                if (menu >= 1) {
                    level = (state == GameState.Flung) ? 0 : SceneManager.GetActiveScene().buildIndex;
                    AudioLord.DestroyLoop(loop, 1.0f);
                    AudioLord.Play("Ding");
                    Fade();
                }
            }

            if (!Input.GetKey(KeyCode.D) && restart > 0) {
                restart = Mathf.Max(0, restart - Time.deltaTime);
                if (restart == 0)
                    restartText.SetActive(true);
            }

            if (!Input.GetKey(KeyCode.S) && menu > 0) {
                menu = Mathf.Max(0, menu - Time.deltaTime);
                if (menu == 0)
                    menuText.SetActive(true);
            }

            menuImage.fillAmount = menu;
            restartImage.fillAmount = restart;
        }


        if (state == GameState.FadeAway) {
            alpha = Mathf.Min(1, alpha + Time.deltaTime);
            curtain.color = Color.Lerp(Color.clear, Color.black, alpha);

            if (alpha == 1) {
                PlayerPrefs.SetInt("CurrentLevel", level);
                PlayerPrefs.SetInt("Credits", 0);
                SceneManager.LoadScene(level);
            }
        }
    }

    void Next() {
        hsList[index].Unselect();
        index++;
        if (index == hsList.Count)
            index = 0;
        hsList[index].Select();
    }

    void Kill() {
        hsList[index].Die();
        hsList.RemoveAt(index);

        if (hsList.Count == 0) {
            press = true;
            menuImage.transform.parent.gameObject.SetActive(true);
            cameras[0].SetActive(false);
            cameras[1].SetActive(true);
            state = GameState.Flung;
        } else {
            if (index == hsList.Count)
                index = 0;

            hsList[index].Select();
        }
    }

    public void End() {
        state = GameState.End;
        end.gameObject.SetActive(true);
        menuImage = end.GetChild(0).GetComponent<Image>();
        restartImage = end.GetChild(1).GetComponent<Image>();
        menuText = end.GetChild(2).gameObject;
        restartText = end.GetChild(3).gameObject;
        AudioLord.Play("Jeah");
        int currentLevel = PlayerPrefs.GetInt("CurrentLevel")+1;
        if (currentLevel > PlayerPrefs.GetInt("Levels")) {
            PlayerPrefs.SetInt("Levels", currentLevel);
        }
    }

    void Fade() {
        state = GameState.FadeAway;
        curtain.gameObject.SetActive(true);
    }
    
    public void GotHit() {
        state = GameState.Hit;
        cameras[1].SetActive(false);
        cameras[2].SetActive(true);
        restart = 0;
        menu = 0;
        menuImage.fillAmount = menu;
        restartImage.fillAmount = restart;
        menuImage.transform.parent.gameObject.SetActive(false);
        AudioLord.DestroyLoop(loop, 0);
        AudioLord.Play("Argh");
    }
}