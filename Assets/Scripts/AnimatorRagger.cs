﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AnimatorRagger : MonoBehaviour {

    Rigidbody[] rbArray;
    Rigidbody rb;
    public Vector3 angleSpeed;
    public float speed;
    List<ARChecker> arcList = new List<ARChecker>();
    ControllerScript cs;
    public AnimationCurve curve;
    bool hit;
    float timeScaler;
    public float slowMo;
    float end = -1;
    public Vector3 flip;

    void Start() {
        GameObject controller = GameObject.Find("Controller");
        if (controller != null)
        cs = controller.GetComponent<ControllerScript>();
        rb = GetComponent<Rigidbody>();
        rbArray = GetComponentsInChildren<Rigidbody>();
        foreach (Rigidbody r in rbArray) {
            ARChecker arc = r.gameObject.AddComponent<ARChecker>();
            arc.ar = this;
            arcList.Add(arc);
        }
    }

    void OnTriggerEnter(Collider c) {
        if (c.tag == "Flipper")
            rb.rotation *= Quaternion.Euler(flip);
    }

    void Update() {
        if (!hit) {
            rb.rotation *= Quaternion.Euler(angleSpeed * Time.deltaTime);
            rb.position += transform.forward * speed * Time.deltaTime;
        } else {
            timeScaler = Mathf.Min(timeScaler + Time.deltaTime / (2 * Time.timeScale), 1);
            Time.timeScale = (1 - slowMo) * curve.Evaluate(timeScaler) + slowMo;

            if (timeScaler == 1 && end < 0) {
                end = 0;
            }

            if (end >= 0) {
                end += Time.deltaTime;

                if (end > 2) {
                    if (cs != null)
                    cs.End();
                    Destroy(this);
                }
            }
        }
    }

    public void GetHit() {
        if (!hit && (cs == null || cs.state == ControllerScript.GameState.Flung)) {
            hit = true;
            if (cs != null)
                cs.GotHit();
            Time.timeScale = slowMo;
            foreach (Rigidbody r in rbArray) {
                r.useGravity = true;
                r.velocity = transform.forward * speed;
                r.angularVelocity = angleSpeed;
            }
            foreach (ARChecker arc in arcList) {
                Destroy(arc);
            }

            Destroy(GetComponent<Animator>());
            Destroy(rb);
        }
    }
}
