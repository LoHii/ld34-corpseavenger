﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuSound : MonoBehaviour {

    Image onSprite;
    Image offSprite;
    Button button;
    bool on = true;

    void Start() {
        onSprite = GetComponent<Image>();
        offSprite = transform.GetChild(0).GetComponent<Image>();
        button = GetComponent<Button>();
        if (AudioLord.lord.play) {
            offSprite.enabled = false;
            on = true;
        } else {
            onSprite.enabled = false;
            button.targetGraphic = offSprite;
            on = false;
        }
    }

	public void Pressed() {
        on = !on;
        if (on) {
            onSprite.enabled = true;
            offSprite.enabled = false;
            button.targetGraphic = onSprite;
        } else {
            onSprite.enabled = false;
            offSprite.enabled = true;
            button.targetGraphic = offSprite;
        }

        AudioLord.Change(on);
    }
}