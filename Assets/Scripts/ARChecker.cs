﻿using UnityEngine;
using System.Collections;

public class ARChecker : MonoBehaviour {
    
    public AnimatorRagger ar;

	void Start () {
        if (GetComponent<Collider>() == null)
            Destroy(this);
	}
	
    void OnCollisionEnter(Collision coll) {
        if (coll.gameObject.tag == "Player") {
            ar.GetHit();
        }
    }
}
