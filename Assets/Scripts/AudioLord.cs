﻿using UnityEngine;
using System.Collections;

public class AudioLord : MonoBehaviour {

    public static AudioLord lord;
    [HideInInspector]
    public Transform holder;
    bool notOne;
    public bool play = true;

    void Awake() {
        if (lord != null) {
            Destroy(this);
        } else
            lord = this;

        DontDestroyOnLoad(lord.gameObject);
        holder = transform.GetChild(1);
    }

    public static void Change(bool doPlay) {
        lord.play = doPlay;
    }

    void Update() {
        if (Time.timeScale != 1)
            notOne = true;

        if (notOne) {
            for (int i = 0; i < holder.childCount; i++)
                holder.GetChild(i).GetComponent<AudioSource>().pitch = Time.timeScale;

            if (Time.timeScale == 1)
                notOne = false;
        }
    }

    public static void Play(string name) {
        if (lord.play) {
            GameObject go = lord.transform.GetChild(0).Find(name).gameObject;
            AudioSource audio = Instantiate(go).GetComponent<AudioSource>();
            audio.Play();
            audio.transform.SetParent(lord.holder);
            Destroy(audio.gameObject, audio.clip.length);
        }
    }

    public static AudioSource PlayLoop(string name) {
        if (lord.play) {
            GameObject go = lord.transform.GetChild(0).Find(name).gameObject;
            AudioSource audio = Instantiate(go).GetComponent<AudioSource>();
            audio.transform.SetParent(lord.holder);
            audio.Play();
            return audio;
        }
        return null;
    }

    public static void DestroyLoop(AudioSource audio, float fadeTime) {
        if (audio != null) {
            GameObject go = audio.gameObject;
            AudioFader af = go.AddComponent<AudioFader>();
            af.fadeTime = fadeTime;
        }
    }
}
